FactoryBot.define do
  factory :car do
    factory :suzuki do
      name "Suzuki Vitara"
      color 0
      weight 1100
      latitude 20.3
      longitude 34.4
      availability true
    end

    factory :toyota do
      name "Toyota RAV4"
      color 3
      weight 1400
      latitude 24.3
      longitude 34.4
      availability true
    end
  end
end
