FactoryBot.define do
  factory :reservation do
    start_date Date.today + 1.day
    end_date Date.today + 3.days
    association :user, factory: :user1
    association :car, factory: :toyota
  end
end
