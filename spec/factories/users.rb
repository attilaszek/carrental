FactoryBot.define do
  factory :user do
  
    factory :user1 do
      first_name "John"
      last_name "Button"
      email "john.button@example.com"
      password "123456"
      password_confirmation "123456"
    end

    factory :user2 do
      first_name "Joe"
      last_name "Russel"
      email "joe.russel@example.com"
      password "abcdefg"
      password_confirmation "abcdefg"
    end
  
  end
end