require "spec_helper"

RSpec.describe Query::Contract::FilterCars do
  let(:params) {
    {
      start_date: Date.today + 2.days,
      end_date: Date.today + 4.days,
      name: "Ford"
    }
  }

  pending "add more examples"

  context 'valid parameters' do
    it 'passes' do
      contract = Query::Contract::FilterCars.new(nil)
      result = contract.validate(params)

      expect( result ).to be_truthy
    end
  end

  context 'invalid parameters' do
    it 'fails with incorrect time interval (start_date > end_date)' do
      contract = Query::Contract::FilterCars.new(nil)
      result = contract.validate(params.merge(end_date: Date.today))

      expect( result ).to be_falsy
    end

    it 'fails with start_date before today' do
      contract = Query::Contract::FilterCars.new(nil)
      result = contract.validate(params.merge(start_date: Date.today - 1.day))

      expect( result ).to be_falsy
    end

  end
end