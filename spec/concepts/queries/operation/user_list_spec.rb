require "spec_helper"

RSpec.describe Query::UserList do

  before (:each) do
    @user = create(:user1)
    role = create(:admin)
    @user.roles << role
  end
  
  it 'returns users with reservations' do
    user1 = create(:user1)
    user2 = create(:user2)
    create(:reservation, user: user1)

    result = Query::UserList.({}, "current_user" => @user)

    expect( result.success? ).to be_truthy
    expect( result["model"].length ).to eq(1)
    expect( result["model"].first ).to eq(user1) 
  end

end