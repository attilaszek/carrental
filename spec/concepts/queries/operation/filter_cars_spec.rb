require "spec_helper"

RSpec.describe Query::FilterCars do

  before (:each) do
    @user = create(:user1)
    role = create(:newuser)
    @user.roles << role
  end


  context 'filter by date' do
    context '1 car reserved, 1 not in the specified interval' do
      it 'return only the free car' do
        reservation = create(:reservation)
        car = create(:suzuki)

        result = Query::FilterCars.({start_date: reservation.end_date,
                                      end_date: reservation.end_date + 1.day},
                                      "current_user" => @user)

        expect( result.success? ).to be_truthy
        expect( result["model"].length ).to eq(1)

      end
    end
  end

  context 'filter by name' do
    it 'return car which name contains the search term' do
      create(:suzuki)
      create(:suzuki)
      create(:suzuki)
      create(:toyota)

      result = Query::FilterCars.({name: "uzu"}, "current_user" => @user)

      expect( result.success? ).to be_truthy 
      expect( result["model"].length ).to eq(3)
    end
  end

  context 'filter by weight' do
    it 'return car which weight is between min and max weight' do
      car1 = create(:suzuki, weight: 1200)
      car2 = create(:toyota, weight: 1400)

      result = Query::FilterCars.({weight_min: 1300, weight_max: 1500}, "current_user" => @user)

      expect( result.success? ).to be_truthy 
      expect( result["model"].length ).to eq(1)
      expect( result["model"].first ).to eq(car2)
    end
  end

  context 'filter by color' do
    it 'return cars with specified color' do
      car1 = create(:suzuki, color: "red")
      car2 = create(:toyota, color: "silver")

      result = Query::FilterCars.({color: "red"}, "current_user" => @user)

      expect( result.success? ).to be_truthy 
      expect( result["model"].length ).to eq(1)
      expect( result["model"].first ).to eq(car1)
    end
  end

end