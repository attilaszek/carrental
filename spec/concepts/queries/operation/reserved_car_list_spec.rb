require "spec_helper"

RSpec.describe Query::ReservedCarList do

  before (:each) do
    @user = create(:user1)
    role = create(:admin)
    @user.roles << role
  end
  
  it 'returns reserved cars' do
    car1 = create(:suzuki)
    car2 = create(:suzuki)
    car3 = create(:toyota)

    user1 = create(:user1)
    user2 = create(:user2)

    create(:reservation, car: car1, user: user1)
    create(:reservation, car: car2, user: user2)
    create(:reservation, car: car3, user: user1)

    result = Query::ReservedCarList.({user_id: user1.id}, "current_user" => @user)

    expect( result.success? ).to be_truthy
    expect( result["model"].length ).to eq(2)
    expect( result["model"] ).to eq([car1, car3]) 
  end

end