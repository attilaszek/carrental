require "spec_helper"

RSpec.describe Reservation::Contract::Create do
  let(:params) {
    {
      start_date: Date.today + 2.days,
      end_date: Date.today + 4.days,
      car_id: create(:toyota).id
    }
  }

  context 'valid parameters' do
    it 'passes' do
      contract = Reservation::Contract::Create.new(Reservation.new)
      result = contract.validate(params)

      expect( result ).to be_truthy
    end
  end

  context 'invalid parameters' do
    it 'fails with incorrect time interval (start_date > end_date)' do
      contract = Reservation::Contract::Create.new(Reservation.new)
      result = contract.validate(params.merge(end_date: Date.today))

      expect( result ).to be_falsy
    end

    it 'fails with start_date before today' do
      contract = Reservation::Contract::Create.new(Reservation.new)
      result = contract.validate(params.merge(start_date: Date.today - 1.day))

      expect( result ).to be_falsy
    end
  
    it 'fails with inexistent car' do
      contract = Reservation::Contract::Create.new(Reservation.new)
      result = contract.validate(params.merge(car_id: params[:car_id] + 1))

      expect( result ).to be_falsy
      expect( contract.errors.messages ).to eq(car_id: ["Car doesn't exist"])
    end

  end
end