require "spec_helper"

RSpec.describe Reservation::Create do

  before (:each) do
    @user = create(:user1)
    newuser_role = create(:newuser)
    @user.roles << newuser_role
  end

  context 'delete own reservation' do
    it 'succeds' do
      reservation = create(:reservation)
      reservation.user_id = @user.id
      reservation.save!

      result = Reservation::Delete.({id: reservation.id}, "current_user" => @user)
      expect( result.success? ).to be_truthy
      expect( Reservation.count ).to eq(0)
    end
  end

end