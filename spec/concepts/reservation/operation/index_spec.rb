require "spec_helper"

RSpec.describe Reservation::Create do
  before (:each) do
    @user1 = create(:user1)
    newuser_role = create(:newuser)
    @user1.roles << newuser_role

    @user2 = create(:user2)
    admin_role = create(:admin)
    @user2.roles << admin_role
    
    create(:reservation, user_id: @user1.id)
    create(:reservation, user_id: @user1.id)
    create(:reservation, user_id: @user2.id)
    create(:reservation, user_id: @user2.id)
  end

  context 'newuser' do
    it "returns the user's reservations" do
      result = Reservation::Index.({}, "current_user" => @user1)

      expect( result.success? ).to be_truthy
      expect( result["model"].length ).to eq(2)
    end
  end

  context 'no user' do
    it 'fails' do
      result = Reservation::Index.({}, "current_user" => nil)

      expect( result.success? ).to be_falsy
    end
  end
end