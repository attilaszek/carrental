require "spec_helper"

RSpec.describe Reservation::Create do
  let (:params) { 
    {
      start_date: Date.today + 2.days,
      end_date: Date.today + 4.days,
      car_id: create(:toyota).id
    }
  }

  before (:each) do
    @user = create(:user1)
    newuser_role = create(:newuser)
    @user.roles << newuser_role
  end

  context 'valid params' do
    it 'creates new reservation' do
      result = Reservation::Create.(params, "current_user" => @user)
      expect( result.success? ).to be_truthy
      expect( result["contract.default"].errors.messages ).to be_empty
      expect( Reservation.count ).to eq(1)
      expect( result["model"].start_date ).to eq(params[:start_date])
      expect( result["model"].end_date ).to eq(params[:end_date])
      expect( result["model"].user_id ).to eq(@user.id)
      expect( result["model"].car_id ).to eq(params[:car_id])
    end
  end

  context 'reservation interval intersects with previous reservation' do
    it 'doesnt create the second reservation' do
      Reservation::Create.(params, "current_user" => @user)
      result = Reservation::Create.(params.merge(end_date: Date.today + 3.days), "current_user" => @user)

      expect( result.success? ).to be_falsy
      expect( Reservation.all.length ).to eq(1)
    end
  end

end