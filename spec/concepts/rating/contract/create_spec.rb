require "spec_helper"

RSpec.describe Rating::Contract::Create do
  
  let(:params) {
    {
      car_id: 3,
      rate: 2      
    }
  }

  context 'valid rating' do
    it 'passes' do
      contract = Rating::Contract::Create.new(Rating.new)
      result = contract.validate(params)

      expect( result ).to be_truthy
    end
  end

  context 'invalid rating' do
    it 'passes' do
      contract = Rating::Contract::Create.new(Rating.new)
      result = contract.validate(params.merge(rate: 0))

      expect( result ).to be_falsy
    end
  end

end