require "spec_helper"

RSpec.describe Rating::Create do
  
  before (:each) do
    @user = create(:user1)
    role = create(:newuser)
    @user.roles << role

    @car = create(:toyota)
  end

  it 'creates new rating' do
    result = Rating::Create.({car_id: @car.id, rate: 2}, "current_user" => @user)

    expect( result.success? ).to be_truthy
    expect( Rating.all.length ).to eq(1)
  end

  it 'updates existing rating' do
    result = Rating::Create.({car_id: @car.id, rate: 2}, "current_user" => @user)
    result = Rating::Create.({car_id: @car.id, rate: 4}, "current_user" => @user)

    expect( result.success? ).to be_truthy
    expect( Rating.all.length ).to eq(1)
    expect( Rating.first.rate ).to eq(4)
  end

end