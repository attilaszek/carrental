require "spec_helper"

RSpec.describe Car::Contract::Create do
  let(:params) {
    {
      name: "Ford Focus",
      color: "red",
      weight: 1200,
      latitude: 34.56,
      longitude: 12.33
    }
  }

  context 'valid parameters' do
    it 'passes' do
      contract = Car::Contract::Create.new(Car.new)
      result = contract.validate(params)

      expect( result ).to be_truthy
    end
  end

  context 'invalid parameters' do
    it 'fails with inexistent color' do
      contract = Car::Contract::Create.new(Car.new)
      result = contract.validate(params.merge(color: "cyan"))

      expect( result ).to be_falsy
    end

    it 'fails with incorrect weight' do
      contract = Car::Contract::Create.new(Car.new)
      result = contract.validate(params.merge(weight: -5))

      expect( result ).to be_falsy
    end

    it 'fails with incorrect latitude' do
      contract = Car::Contract::Create.new(Car.new)
      result = contract.validate(params.merge(latitude: -90.5))

      expect( result ).to be_falsy
    end

    it 'fails with incorrect longitude' do
      contract = Car::Contract::Create.new(Car.new)
      result = contract.validate(params.merge(longitude: 180.5))

      expect( result ).to be_falsy
    end
  end
end