require "spec_helper"

RSpec.describe Car::Index do
  
  it 'returns user list' do
    user = create(:user1)
    newuser_role = create(:newuser)
    user.roles << newuser_role

    car1 = create(:suzuki)
    car2 = create(:toyota)


    result = Car::Index.({}, "current_user" => user)

    expect( result.success? ).to be_truthy
    expect( result["model"].length ).to eq(2)
    expect( result["model"] ).to include(car1)
    expect( result["model"] ).to include(car2)
  end

end