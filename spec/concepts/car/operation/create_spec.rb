require "spec_helper"

RSpec.describe Car::Create do
  let (:params) { 
    {
      name: "Ford Focus",
      color: "red",
      weight: 1200,
      latitude: 34.56,
      longitude: 12.33
    }
  }

  before (:each) do
    @user = create(:user1)
    newuser_role = create(:admin)
    @user.roles << newuser_role
  end

  context 'valid params' do
    it 'creates new car' do
      result = Car::Create.(params, "current_user" => @user)
      expect( result.success? ).to be_truthy
      expect( result["contract.default"].errors.messages ).to be_empty
      expect( Car.count ).to eq(1)
      expect( result["model"].name ).to eq(params[:name])
      expect( result["model"].color ).to eq(params[:color])
      expect( result["model"].weight ).to eq(params[:weight])
      expect( result["model"].latitude ).to eq(params[:latitude])
      expect( result["model"].longitude ).to eq(params[:longitude])
      expect( result["model"].availability ).to eq(true)
    end

    it 'creates 2 new users' do
      Car::Create.(params, "current_user" => @user)
      Car::Create.(params.merge(name: "Toyota RAV4"), "current_user" => @user)

      expect( Car.all.length ).to eq(2)
      expect( Car.all.map {|car| car.name} ).to match_array(["Toyota RAV4", params[:name]])
    end
  end

  context 'invalid params' do
    it 'fails with invalid color' do
      result = Car::Create.(params.merge(color: "cyan"), "current_user" => @user)
      expect( result.success? ).to be_falsy
      expect( result["contract.default"].errors.messages.length ).to eq(1)
    end
  end
end