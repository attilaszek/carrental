require "spec_helper"

RSpec.describe Car::Show do

  it 'returns car' do
    user = create(:user1)
    newuser_role = create(:newuser)
    user.roles << newuser_role

    car = create(:car)

    result = Car::Show.({id: car.id}, "current_user" => user)

    expect( result.success? ).to be_truthy
    expect( result["model"] ).to eq(car)
  end

end