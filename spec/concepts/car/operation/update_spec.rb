require "spec_helper"

RSpec.describe Car::Update do

  before (:each) do
    @user = create(:user1)
    newuser_role = create(:admin)
    @user.roles << newuser_role
  end

  context 'valid params' do
    it 'updates car' do
      car = create(:toyota)
      
      result = Car::Update.({id: car.id, weight: 1700}, "current_user" => @user)
      expect( result.success? ).to be_truthy
      expect( result["contract.default"].errors.messages ).to be_empty
      expect( Car.count ).to eq(1)

      expect( result["model"].name ).to eq(car.name)
      expect( result["model"].color ).to eq(car.color)
      expect( result["model"].weight ).to eq(1700)
      expect( result["model"].latitude ).to eq(car.latitude)
      expect( result["model"].longitude ).to eq(car.longitude)
      expect( result["model"].availability ).to eq(car.availability)
    end
  end

  context 'invalid params' do
    it 'fails' do
      car = create(:toyota)
      
      result = Car::Update.({id: car.id, weight: -5}, "current_user" => @user)
      expect( result.success? ).to be_falsy
      expect( result["contract.default"].errors.messages.length ).to eq(1)
      expect( Car.count ).to eq(1)
      expect( result["model"].name ).to eq(car.name)
      expect( result["model"].color ).to eq(car.color)
      expect( result["model"].weight ).to eq(car.weight)
      expect( result["model"].latitude ).to eq(car.latitude)
      expect( result["model"].longitude ).to eq(car.longitude)
      expect( result["model"].availability ).to eq(car.availability)
    end
  end

end