require "spec_helper"

RSpec.describe Car::Delete do

  before (:each) do
    @user = create(:user1)
    role = create(:admin)
    @user.roles << role
  end


  context 'existing car' do
    it 'deletes the car' do
      car = create(:toyota)

      result = Car::Delete.({id: car.id}, "current_user" => @user)
      expect( result.success? ).to be_truthy
      expect{ Car.find(car.id) }.to raise_error(ActiveRecord::RecordNotFound)
    end
  end

  context "car doesn't exist" do
    it 'fails' do
      car = create(:toyota)
      result = Car::Delete.({id: car.id+1}, "current_user" => @user)
      expect( result.success? ).to be_falsy
      expect( Car.all ).to include(car)
    end
  end
end