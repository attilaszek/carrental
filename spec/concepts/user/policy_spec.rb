require "spec_helper"

RSpec.describe UserPolicy do
  context 'newuser?' do
    context 'positive' do
      it 'returns true' do
        user = create(:user1)
        newuser_role = create(:newuser)
        user.roles << newuser_role

        policy = UserPolicy.new(user, User.new)

        expect( policy.newuser? ).to be_truthy
      end
    end

    context 'negative' do
      it 'returns false' do
        user = create(:user1)

        policy = UserPolicy.new(user, User.new)

        expect( policy.newuser? ).to be_falsy
      end
    end
  end

  context 'admin?' do
    context 'positive' do
      it 'returns true' do
        user = create(:user1)
        admin_role = create(:admin)
        user.roles << admin_role

        policy = UserPolicy.new(user, User.new)

        expect( policy.admin? ).to be_truthy
      end
    end

    context 'negative' do
      it 'returns false' do
        user = create(:user1)

        policy = UserPolicy.new(user, User.new)

        expect( policy.admin? ).to be_falsy
      end
    end
  end

  context 'can_access?' do
    context 'positive' do
      context 'admin user' do
        it 'returns true' do
          user1 = create(:user1)
          user2 = create(:user2)
          admin_role = create(:admin)
          user1.roles << admin_role

          policy = UserPolicy.new(user1, user2)

          expect( policy.can_access? ).to be_truthy
        end
      end

      context 'self access' do
        it 'returns true' do
          user1 = create(:user1)
          newuser_role = create(:newuser)
          user1.roles << newuser_role

          policy = UserPolicy.new(user1, user1)

          expect( policy.can_access? ).to be_truthy
        end
      end
    end

    context 'negative' do
      context 'does not have :admin or :newuser role' do
        it 'returns false' do
          user1 = create(:user1)

          policy = UserPolicy.new(user1, user1)

          expect( policy.can_access? ).to be_falsy
        end
      end

      context 'newuser cannot access other users' do
        it 'returns false' do
          user1 = create(:user1)
          user2 = create(:user2)
          newuser_role = create(:newuser)
          user1.roles << newuser_role

          policy = UserPolicy.new(user1, user2)

          expect( policy.can_access? ).to be_falsy
        end
      end
    end
  end
end