require "spec_helper"

RSpec.describe User::Show do

  it 'returns user' do
    user1 = create(:user1)
    user2 = create(:user2)
    amdin_role = create(:admin)
    user2.roles << amdin_role

    result = User::Show.({id: user1.id}, "current_user" => user2)

    expect( result.success? ).to be_truthy
    expect( result["model"] ).to eq(user1)
  end

end