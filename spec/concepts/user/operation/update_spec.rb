require "spec_helper"

RSpec.describe User::Update do

  context 'valid params' do
    it 'updates user' do
      user = create(:user1)
      amdin_role = create(:admin)
      user.roles << amdin_role
      
      result = User::Update.({id: user.id, first_name: "ASD"}, "current_user" => user)

      expect( result.success? ).to be_truthy
      expect( result["contract.default"].errors.messages ).to be_empty
      expect( User.count ).to eq(1)
      expect( result["model"].first_name ).to eq("ASD")
      expect( result["model"].last_name ).to eq(user.last_name)
      expect( result["model"].email ).to eq(user.email)
      expect( result["model"].password_digest ).to eq(user.password_digest)
    end
  end

  context 'invalid params' do
    it 'nothing hapens' do
      user = create(:user1)
      amdin_role = create(:admin)
      user.roles << amdin_role
      
      result = User::Update.({id: user.id, email: "asd@gmail.com"}, "current_user" => user)
      expect( result.success? ).to be_truthy
      expect( result["contract.default"].errors.messages ).to be_empty
      expect( User.count ).to eq(1)
      expect( result["model"].first_name ).to eq(user.first_name)
      expect( result["model"].last_name ).to eq(user.last_name)
      expect( result["model"].email ).to eq(user.email)
      expect( result["model"].password_digest ).to eq(user.password_digest)
    end
  end

end