require "spec_helper"

RSpec.describe User::Authorize do

  context 'valid token' do
    it 'returns the user' do
      user = create(:user1)
      result = User::Login.(email: user.email, password: user.password)

      headers = Hash.new
      headers['Authorization'] = result["token"]

      result = User::Authorize.(headers: headers)

      expect( result.success? ).to be_truthy
      expect( result["model"] ).to eq(user)
    end
  end

  context 'invalid token' do
    it 'fails with error message' do
      user = create(:user1)
      result = User::Login.(email: user.email, password: user.password)

      headers = Hash.new
      headers['Authorization'] = result["token"] + "a"

      result = User::Authorize.(headers: headers)

      expect( result.success? ).to be_falsy
      expect( result['errors'] ).to include({token: 'Missing or invalid token'})
    end
  end

  context 'no token' do
    it 'fails with error message' do
      result = User::Authorize.()

      expect( result.success? ).to be_falsy
      expect( result['errors'] ).to include({token: 'Missing or invalid token'})
    end
  end
end