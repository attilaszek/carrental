require "spec_helper"

RSpec.describe User::Create do
  let (:params) { 
    { 
      first_name: "John",
      last_name: "Russel",
      email: "john.russel@example.com",
      password: "123456",
      password_confirmation: "123456"
    }
  }

  context 'valid params' do
    it 'creates new user' do
      result = User::Create.(params)
      expect( result.success? ).to be_truthy
      expect( result["contract.default"].errors.messages ).to be_empty
      expect( User.count ).to eq(1)
      expect( result["model"].first_name ).to eq(params[:first_name])
      expect( result["model"].last_name ).to eq(params[:last_name])
      expect( result["model"].email ).to eq(params[:email])
      expect( result["model"].has_role?(:newuser) ).to be_truthy
    end

    it 'creates 2 new users' do
      User::Create.(params)
      User::Create.(params.merge(email: "newemail@example.com"))

      expect( User.all.length ).to eq(2)
      expect( User.all.map {|user| user.email} ).to match_array(["newemail@example.com", params[:email]])
    end
  end

  context 'invalid params' do
    it 'fails with invalid email' do
      result = User::Create.(params.merge(email: "asd@"))
      expect( result.success? ).to be_falsy
      expect( result["auth_token"]).to be_nil
      expect( result["contract.default"].errors.messages.length ).to eq(1)
      expect( result["contract.default"].errors.messages ).to include(email: ["is in invalid format"])
    end
  end
end