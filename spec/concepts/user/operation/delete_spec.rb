require "spec_helper"

RSpec.describe User::Delete do

  before (:each) do
    @user = create(:user1)
    role = create(:admin)
    @user.roles << role
  end

  context 'existing user' do
    it 'deletes the user' do
      result = User::Delete.({id: @user.id}, "current_user" => @user)
      expect( result.success? ).to be_truthy
      expect{ User.find(@user.id) }.to raise_error(ActiveRecord::RecordNotFound)
    end
  end

  context "user doesn't exist" do
    it 'fails' do
      result = User::Delete.({id: @user.id + 1}, "current_user" => @user)
      expect( result.success? ).to be_falsy
      expect( User.all ).to include(@user)
    end
  end
end