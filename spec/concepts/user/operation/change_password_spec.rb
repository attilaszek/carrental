require "spec_helper"

RSpec.describe User::ChangePassword do
  
  context 'with valid password & confirmation' do
    it 'changes password' do
      user = create(:user1)
      amdin_role = create(:admin)
      user.roles << amdin_role

      result = User::ChangePassword.({id: user.id, password: "abcdef", password_confirmation: "abcdef"}, "current_user" => user)
      expect( result.success? ).to be_truthy
      expect( result["contract.default"].errors.messages ).to be_empty
      expect( User.count ).to eq(1)
      expect( result["model"].first_name ).to eq(user.first_name)
      expect( result["model"].last_name ).to eq(user.last_name)
      expect( result["model"].email ).to eq(user.email)
      expect( result["model"].password_digest ).not_to eq( user.password_digest )
    end
  end

  context 'with invalid password' do
    it 'does nothing' do
      user = create(:user1)
      amdin_role = create(:admin)
      user.roles << amdin_role

      result = User::ChangePassword.({id: user.id, password: "abcdef", password_confirmation: "abcdefg"}, "current_user" => user)
      expect( result.success? ).to be_falsy
      expect( result["contract.default"].errors.messages.length ).to eq(1)
      expect( User.count ).to eq(1)
      expect( result["model"].first_name ).to eq(user.first_name)
      expect( result["model"].last_name ).to eq(user.last_name)
      expect( result["model"].email ).to eq(user.email)
      expect( result["model"].password_digest ).to eq( user.password_digest )
    end
  end

end