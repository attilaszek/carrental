require "spec_helper"

RSpec.describe User::Index do
  
  it 'returns user list' do
    user1 = create(:user1)
    user2 = create(:user2)
    amdin_role = create(:admin)

    user2.roles << amdin_role 

    result = User::Index.({}, "current_user" => user2)

    expect( result.success? ).to be_truthy
    expect( result["model"].length ).to eq(2)
    expect( result["model"] ).to include(user1)
    expect( result["model"] ).to include(user2)
  end

end