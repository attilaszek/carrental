require "spec_helper"

RSpec.describe User::Login do

  context 'valid credentials' do
    it 'return token' do
      user = create(:user1)

      result = User::Login.(email: user.email, password: user.password)

      expect( result.success? ).to be_truthy
      expect( result["token"] ).not_to be_nil
    end
  end

  context 'invalid credentials' do
    context 'not registered' do
      it 'fails' do
        user = create(:user1)

        result = User::Login.(email: "inexistent@example.com", password: user.password)

        expect( result.success? ).to be_falsy
        expect( result["token"] ).to be_nil
        expect( result["errors"].length ).to eql(1)
        expect( result["errors"] ).to include({email: 'Invalid email'})
      end
    end

    context 'incorrect password' do
      it 'fails' do
        user = create(:user1)

        result = User::Login.(email: user.email, password: "incorrect")

        expect( result.success? ).to be_falsy
        expect( result["token"] ).to be_nil
        expect( result["errors"].length ).to eql(1)
        expect( result["errors"] ).to include({password: 'Invalid password'})
      end
    end
  end
end