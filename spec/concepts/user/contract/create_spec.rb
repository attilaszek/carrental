require "spec_helper"

RSpec.describe User::Create do
  let (:params) { 
    { 
      first_name: "John",
      last_name: "Russel",
      email: "john.russel@example.com",
      password: "123456",
      password_confirmation: "123456"
    }
  }

  context 'valid params' do
    it 'is OK' do
      contract = User::Contract::Create.new(User.new)
      result = contract.validate(params)
      expect( result ).to be_truthy
      expect( contract.errors.messages ).to be_empty
    end
  end

  context 'invalid params' do
    it 'fails with invalid email' do
      contract = User::Contract::Create.new(User.new)
      result = contract.validate(params.merge(email: "asd@"))
      expect( result ).to be_falsy
      expect( contract.errors.messages.length ).to eq(1)
      expect( contract.errors.messages ).to include(email: ["is in invalid format"])
    end

    it 'fails without first_name' do
      contract = User::Contract::Create.new(User.new)
      result = contract.validate(params.merge(first_name: nil))
      expect( result ).to be_falsy
      expect( contract.errors.messages.length ).to eq(1)
      expect( contract.errors.messages ).to include(first_name: ["must be filled"])
    end

    it 'fails without last_name' do
      contract = User::Contract::Create.new(User.new)
      result = contract.validate(params.merge(last_name: nil))
      expect( result ).to be_falsy
      expect( contract.errors.messages.length ).to eq(1)
      expect( contract.errors.messages ).to include(last_name: ["must be filled"])
    end

    it 'fails with short password' do
      contract = User::Contract::Create.new(User.new)
      result = contract.validate(params.merge(password: "asd12", password_confirmation: "asd12"))
      expect( result ).to be_falsy
      expect( contract.errors.messages.length ).to eq(1)
      expect( contract.errors.messages ).to include(password: ["size cannot be less than 6"])
    end

    it 'fails because password_confirmation' do
      contract = User::Contract::Create.new(User.new)
      result = contract.validate(params.merge(password_confirmation: "123457"))
      expect( result ).to be_falsy
      expect( contract.errors.messages.length ).to eq(1)
      expect( contract.errors.messages ).to include(password_confirmation: ["must equal password"])
    end

    it 'fails because already taken email' do
      user = create(:user1)
      
      contract = User::Contract::Create.new(User.new)
      result = contract.validate(params.merge(email: user.email))
      
      expect( result ).to be_falsy
      expect( contract.errors.messages.length ).to eq(1)
      expect( contract.errors.messages ).to include(email: ["has already been taken"])
    end

    it 'fails with 2 causes' do
      result = User::Create.(params)

      contract = User::Contract::Create.new(User.new)
      result = contract.validate(params.merge(last_name: "New", password: "123457"))

      expect( result ).to be_falsy
      expect( contract.errors.messages.length ).to eq(2)
      expect( contract.errors.messages ).to include(
        email: ["has already been taken"],
        password_confirmation: ["must equal password"]
      )
    end
  end
end