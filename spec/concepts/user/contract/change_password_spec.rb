require "spec_helper"

RSpec.describe User::Contract::ChangePassword do

  context 'valid' do
    it 'returns true' do
      contract = User::Contract::ChangePassword.new(User.new)
      result = contract.validate(password: "aaaaaa", password_confirmation: "aaaaaa")

      expect( result ).to be_truthy
      expect( contract.errors.messages.length ).to eq(0)
    end
  end

  context 'invalid' do
    context 'too short' do
      it 'return false' do
        contract = User::Contract::ChangePassword.new(User.new)
        result = contract.validate(password: "aaaaa", password_confirmation: "aaaaa")

        expect( result ).to be_falsy
        expect( contract.errors.messages.length ).to eq(1)
        expect( contract.errors.messages ).to include(password: ["size cannot be less than 6"])
      end
    end

    it 'fails fails confirmation' do
      contract = User::Contract::ChangePassword.new(User.new)
      result = contract.validate(password: "aaaaaa", password_confirmation: "bbbbbb")

      expect( result ).to be_falsy
      expect( contract.errors.messages.length ).to eq(1)
      expect( contract.errors.messages ).to include(password_confirmation: ["must be equal to aaaaaa"])
    end
  end

end