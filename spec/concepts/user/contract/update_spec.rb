require "spec_helper"

RSpec.describe User::Update do

  context 'valid first_name' do
    it 'returns true' do
      contract = User::Contract::Update.new(User.new)
      result = contract.validate(first_name: "Martin")
      
      expect( result ).to be_truthy
      expect( contract.errors.messages.length ).to eq(0)
    end
  end

  context 'valid last_name' do
    it 'returns true' do
      contract = User::Contract::Update.new(User.new)
      result = contract.validate(last_name: "Button")

      expect( result ).to be_truthy
      expect( contract.errors.messages.length ).to eq(0)
    end
  end

end