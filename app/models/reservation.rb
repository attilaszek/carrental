class Reservation < ApplicationRecord
  belongs_to :user
  belongs_to :car

  def car_name
    car.name
  end

  def user_name
    user.first_name + " " + user.last_name
  end
end
