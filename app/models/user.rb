class User < ApplicationRecord
  rolify
  has_secure_password

  has_many :reservations, dependent: :destroy

  def admin?
    has_role? :admin
  end
end
