class Car < ApplicationRecord
  enum color: [ :red, :green, :blue, :black, :white, :silver]

  has_attached_file :image
  validates_attachment_content_type :image, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]


  has_many :reservations
  has_many :ratings

  delegate :each, to: :reservations, prefix: true


  def check_availability(start_date, end_date)
    reservations_each do |reservation|
      if (start_date..end_date).overlaps?(reservation.start_date..reservation.end_date)
        return false
      end
    end
    true
  end

  def rate_average
    ratings.average(:rate)
  end
end
