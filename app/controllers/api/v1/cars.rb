module API  
  module V1
    class Cars < Grape::API
      include API::V1::Defaults

      resource :cars do

        desc "Show car", headers: Base.headers_definition
        params do
          requires :id, type: Integer
        end
        get 'show' do
          authorize_request
          result = Car::Show.(params, "current_user" => current_user)

          if result.success?
            result["model"].image = URI.join(request.url, result["model"].image.url)
            result["model"]
          else
            format_errors(result)
          end
        end

        desc "Car list", headers: Base.headers_definition
        get 'index' do
          authorize_request
          result = Car::Index.({}, "current_user" => current_user)
          if result.success?
            result["model"]
          else
            format_errors(result)
          end
        end

        desc "Create car", headers: Base.headers_definition
        params do
          requires :name, type: String
          requires :color, type: String
          requires :weight, type: Integer
          requires :latitude, type: Float
          requires :longitude, type: Float
          requires :image, type: Rack::Multipart::UploadedFile, :desc => "Image file."
        end
        post 'new' do
          authorize_request
          result = Car::Create.(params, "current_user" => current_user)

          if result.success?
            result["model"]
          else
            format_errors(result)
          end
        end

        desc "Update car", headers: Base.headers_definition
        params do
          requires :id, type: Integer
          optional :name, type: String
          optional :color, type: String
          optional :weight, type: Integer
          optional :latitude, type: Float
          optional :longitude, type: Float
          optional :image, type: Rack::Multipart::UploadedFile, :desc => "Image file."
        end
        put 'edit' do
          authorize_request
          result = Car::Update.(params, "current_user" => current_user)

          if result.success?
            result["model"]
          else
            format_errors(result)
          end
        end

        desc "Delete car", headers: Base.headers_definition
        params do
          requires :id, type: Integer
        end
        delete do
          authorize_request
          result = Car::Delete.(params, "current_user" => current_user)
          
          if result.success?
            result["model"]
          else
            format_errors(result)
          end
        end

        desc "Get colors", headers: Base.headers_definition
        get "colors" do
          authorize_request
          Car.colors
        end

      end
    end
  end
end