module API
  module V1
    class Ratings < Grape::API
      include API::V1::Defaults

      resource :ratings do
      
        desc 'add rating', headers: Base.headers_definition
        params do
          requires :car_id, type: Integer
          requires :rate, type: Integer
        end
        post do
          authorize_request
          result = Rating::Create.(params, "current_user" => current_user)

          if result.success? 
            result["model"]
          else
            format_errors(result)
          end
        end

      end
    end
  end
end