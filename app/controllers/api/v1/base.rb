module API  
  module V1
    class Base < Grape::API

      def self.headers_definition
        {
          "Authorization" => {
            description: "Validates your identity",
            required: true 
          }  
        }
      end

      helpers do
        attr_reader :current_user

        private

        def authorize_request
          result = User::Authorize.(headers: request.headers)
          @current_user = result["model"]
          error!('401 Unauthorized', 401) unless @current_user
        end

        def format_errors(result)
          if result["errors"] and result["errors"].length > 0
             error!(result["errors"], 404)
          end
          contract = result["contract.default"]
          if contract and contract.errors.messages and contract.errors.messages.length > 0
             error!(contract.errors.messages, 404)
          end
          error!("Unauthorized", 404)
        end
      end

    	mount API::V1::Users
      mount API::V1::Cars
      mount API::V1::Reservations
      mount API::V1::Queries
      mount API::V1::Ratings
      # mount API::V1::AnotherResource

      add_swagger_documentation(
        api_version: "v1",
        hide_documentation_path: true,
        mount_path: "/api/v1/swagger_doc",
        hide_format: true
      )
    end
  end
end  