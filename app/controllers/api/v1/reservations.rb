module API  
  module V1
    class Reservations < Grape::API
      include API::V1::Defaults

      resource :reservations do

        desc "List reservations", headers: Base.headers_definition
        params do
          optional :car_id, type: Integer
          optional :user_id, type: Integer
        end
        get 'index' do
          authorize_request
          result = Reservation::Index.(params, "current_user" => current_user)

          if result.success?
            result["model"]
          else
            format_errors(result)
          end
        end

        desc "Create reservation", headers: Base.headers_definition
        params do
          requires :start_date, type: Date
          requires :end_date, type: Date
          requires :car_id, type: Integer
        end
        post 'new' do
          authorize_request
          result = Reservation::Create.(params, "current_user" => current_user)

          if result.success?
            result["model"]
          else
            format_errors(result)
          end
        end

        desc "Delete reservation", headers: Base.headers_definition
        params do
          requires :id, type: Integer
        end
        delete do
          authorize_request
          result = Reservation::Delete.(params, "current_user" => current_user)

          if result.success?
            result["model"]
          else
            format_errors(result)
          end
        end

      end
    end
  end
end