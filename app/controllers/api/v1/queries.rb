module API
  module V1
    class Queries < Grape::API
      include API::V1::Defaults

      resource :query do

        desc "user list", headers: Base.headers_definition
        get 'user_list' do
          authorize_request
          result = Query::UserList.({}, "current_user" => current_user)

          if result.success?
            result["model"]
          else
            format_errors(result)
          end
        end

        desc "reserved cars", headers: Base.headers_definition
        params do
          requires :user_id, type: Integer
        end
        get 'reserved_cars' do
          authorize_request
          result = Query::ReservedCarList.({user_id: params[:user_id]}, "current_user" => current_user)
          
          if result.success?
            result["model"]
          else
            format_errors(result)
          end
        end 

        desc "filter cars", headers: Base.headers_definition
        params do
          optional :start_date, type: Date
          optional :end_date, type: Date
          optional :name, type: String
          optional :weight_min, type: Integer
          optional :weight_max, type: Integer
          optional :color, type: String
          all_or_none_of :start_date, :end_date, message: "all params are required or none is required"
          optional :min_rate, type: Integer
          optional :max_rate, type: Integer
        end
        get 'filter_cars' do
          authorize_request
          result = Query::FilterCars.(params, "current_user" => current_user)

          if result.success?
            result["model"]
          else
            format_errors(result)
          end
        end

      end
    end
  end
end