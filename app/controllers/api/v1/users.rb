module API  
  module V1
    class Users < Grape::API
      include API::V1::Defaults

      resource :users do

        desc "Login"
        params do
          requires :email, type: String
          requires :password, type: String
        end
        post "login" do
          result = User::Login.(params)

          if result.success?
            { token: result["token"] }
          else
            format_errors(result)
          end
        end

        desc "Sign up"
        params do
          requires :first_name, type: String
          requires :last_name, type: String
          requires :email, type: String
          requires :password, type: String
          requires :password_confirmation, type: String
        end
        post "signup" do
          result = User::Create.(params)

          if result.success?
            result = User::Login.({email: params[:email], password: params[:password]})
            { token: result["token"] }
          else
            format_errors(result)
          end
        end

        desc "Get current user"
        get "get_current_user" do
          authorize_request
          if @current_user
            @current_user
          else
            error!('401 Unauthorized', 401) unless @current_user
          end
        end

        desc "Change password", headers: Base.headers_definition
        params do
          requires :id, type: Integer
          requires :password, type: String
          requires :password_confirmation, type: String
        end
        put "change_password" do
          authorize_request
          result = User::ChangePassword.(params, "current_user" => current_user)

          if result.success?
            result["model"]
          else
            format_errors(result)
          end
        end

        desc "Update", headers: Base.headers_definition
        params do
          requires :id, type: Integer
          optional :first_name, type: String
          optional :last_name, type: String
        end
        put "update" do
          authorize_request
          result = User::Update.(params, "current_user" => current_user)

          if result.success?
            result["model"]
          else
            format_errors(result)
          end
        end

        desc "User list", headers: Base.headers_definition
        get "index" do
          authorize_request

          result = User::Index.({}, "current_user" => current_user)

          if result.success?
            result["model"]
          else
            format_errors(result)
          end
        end

        desc "Show user", headers: Base.headers_definition
        params do
          requires :id, type: Integer
        end
        get "show" do
          authorize_request
          result = User::Show.(params, "current_user" => current_user)
          
          if result.success?
            result["model"]
          else
            format_errors(result)
          end
        end

        desc "Delete user", headers: Base.headers_definition
        params do
          requires :id, type: Integer
        end
        delete do
          authorize_request
          result = User::Delete.(params, "current_user" => current_user)
          if result.success?
            true
          else
            format_errors(result)
          end
        end

        desc "Add manager role", headers: Base.headers_definition
        params do
          requires :id, type: Integer
        end
        put 'add_admin_role' do
          authorize_request
          result = User::AddAdminRole.(params, "current_user" => current_user)
          if result.success?
            true
          else
            format_errors(result)
          end
        end
      
        desc "Remove manager role", headers: Base.headers_definition
        params do
          requires :id, type: Integer
        end
        put 'remove_admin_role' do
          authorize_request
          result = User::RemoveAdminRole.(params, "current_user" => current_user)
          if result.success?
            true
          else
            format_errors(result)
          end
        end

      end

  	end
  end
end