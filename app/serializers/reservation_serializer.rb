class ReservationSerializer < ActiveModel::Serializer
  attributes :id, :car_id, :user_id, :start_date, :end_date, :car_name, :user_name
end