class CarSerializer < ActiveModel::Serializer
  attributes :id, :name, :color, :weight, :latitude, :longitude, :rate_average, :image

end