class UserSerializer < ActiveModel::Serializer
  attributes :id, :email, :first_name, :last_name, :admin

  def admin
    object.admin?
  end
end