require 'trailblazer'

class Car::Index < Trailblazer::Operation
  step :model!
  step Policy::Pundit( CarPolicy, :newuser? )

  def model!(options, **)
    options["model"] = Car.all
  end
end