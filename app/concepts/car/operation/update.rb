require 'trailblazer'

class Car::Update < Trailblazer::Operation
  step Model( Car, :find_by)
  step Policy::Pundit( CarPolicy, :admin? )
  step Contract::Build( constant: Car::Contract::Update )
  step Contract::Validate( )
  step Contract::Persist( )
end