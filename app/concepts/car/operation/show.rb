require "trailblazer"

class Car::Show < Trailblazer::Operation
  step Model(Car, :find_by)
  failure :report_record_not_found!
  step Policy::Pundit( CarPolicy, :newuser? )

  def report_record_not_found!(options, **)
    options["errors"] = [{model: "not found"}]
  end
end