require "trailblazer"

class Car::Delete < Trailblazer::Operation
  step Model(Car, :find_by)
  failure :report_record_not_found!
  step Policy::Pundit( CarPolicy, :admin? )
  step :destroy!

  def destroy!(options, model:, **)
    model.destroy
  end

  def report_record_not_found!(options, **)
    options["errors"] = [{model: "not found"}]
  end
end