require "trailblazer"

class Car::Create < Trailblazer::Operation
  step Model( Car, :new )
  step Policy::Pundit( CarPolicy, :admin? )
  step Contract::Build( constant: Car::Contract::Create )
  step Contract::Validate( )
  step :set_default_availability!
  step Contract::Persist( )

  def set_default_availability!(options, model:, **)
    model.availability = true
    true
  end

end