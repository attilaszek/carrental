require 'reform/form/dry'

module Car::Contract
  class Update < Reform::Form
    feature Reform::Form::Dry

    property :name
    property :color
    property :weight
    property :latitude
    property :longitude
    property :image


    validation :default do

      required(:name).maybe(type?: String)
      required(:color).maybe(type?: String, included_in?: Car.colors.keys)
      required(:weight).maybe(type?: Integer, gt?: 0)
      required(:latitude).maybe(type?: Float, gteq?: -90, lteq?: 90)
      required(:longitude).maybe(type?: Float, gteq?: -180, lteq?: 180)

      optional(:image).maybe
    end

  end
end