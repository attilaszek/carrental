require 'reform/form/dry'

module Car::Contract
  class Create < Reform::Form
    feature Reform::Form::Dry

    property :name
    property :color
    property :weight
    property :latitude
    property :longitude
    property :image

    validation :default do

      required(:name).filled(type?: String)
      required(:color).value(type?: String, included_in?: Car.colors.keys)
      required(:weight).value(type?: Integer, gt?: 0)
      required(:latitude).value(type?: Float, gteq?: -90, lteq?: 90)
      required(:longitude).value(type?: Float, gteq?: -180, lteq?: 180)

      required(:image).filled

    end

  end
end