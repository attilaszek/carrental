require "trailblazer"

class Rating::Create < Trailblazer::Operation
  step :model!
  step Policy::Pundit( MyPolicy, :newuser? )

  step Contract::Build( constant: Rating::Contract::Create )
  step Contract::Validate( )
  step :set_user!
  step Contract::Persist( )

  def model!(options, params:, **)
    options["model"] = Rating.where(car_id: params[:car_id]).where(user_id: options["current_user"]).first
    if options["model"].nil?
      options["model"] = Rating.new
    end
    true
  end

  def set_user!(options, **)
    options["model"].user_id = options["current_user"].id
  end
end