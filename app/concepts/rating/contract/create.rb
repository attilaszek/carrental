require 'reform/form/dry'

module Rating::Contract
  class Create < Reform::Form
    feature Reform::Form::Dry

    property :car_id
    property :rate

    validation do
      required(:car_id).filled(type?: Integer)
      required(:rate).filled(type?: Integer, gteq?: 1, lteq?: 5)       
    end

  end
end