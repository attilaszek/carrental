class ReservationPolicy < MyPolicy

  def my_reservation?
    newuser? and @model.user_id == @user.id
  end
  
end