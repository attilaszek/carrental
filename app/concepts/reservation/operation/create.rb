require 'trailblazer'

class Reservation::Create < Trailblazer::Operation
  step Model( Reservation, :new )
  step Policy::Pundit( ReservationPolicy, :newuser? )
  step Contract::Build( constant: Reservation::Contract::Create )
  step Contract::Validate( )
  step :check_interval!
  step :set_user!
  step Contract::Persist( )

  def check_interval!(options, params:, **)
    options["car"] = Car.find(params[:car_id])
    options["car"].check_availability(params[:start_date], params[:end_date])
  end

  def set_user!(options, model:, **)
    model.user_id = options["current_user"].id
  end
end