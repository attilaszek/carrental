require 'trailblazer'

class Reservation::Index < Trailblazer::Operation
  step :model!

  def model!(options, current_user:, params:, **)
    return false if current_user.nil?
    if current_user.admin? and (params[:user_id].present? or params[:car_id].present?)
      options["model"] = Reservation.all
      if params[:user_id].present?
        options["model"] = options["model"].where(user_id: params[:user_id])
      end
      if params[:car_id].present?
        options["model"] = options["model"].where(car_id: params[:car_id])
      end
      options["model"]
    else
      options["model"] = Reservation.where(user_id: current_user.id)
    end 
  end
end