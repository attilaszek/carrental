require 'trailblazer'

class Reservation::Delete < Trailblazer::Operation
  step Model(Reservation, :find_by)
  step Policy::Pundit( ReservationPolicy, :my_reservation? )
  step :destroy!

  def destroy!(options, model:, **)
    if model.start_date > Date.today
      model.destroy
    else
      options["errors"] = [{delete: "Cannot cancel this reservation"}]
      false
    end
  end
end