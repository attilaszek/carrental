require 'reform/form/dry'

module Reservation::Contract
  class Create < Reform::Form
    feature Reform::Form::Dry

    property :start_date
    property :end_date
    #property :user_id
    property :car_id

    validation do
  
      configure do
        config.messages_file = 'config/error_messages.yml'
        config.namespace = :reservation

        def resource_exists?(klass, value)
          klass.find_by(id: value).present?
        end
      end

      required(:start_date).filled(type?: Date, gteq?: Date.today)
      required(:end_date).filled(type?: Date)
  
      validate(correct_time_interval?: [:start_date, :end_date]) do |start_date, end_date|
        start_date <= end_date
      end

      #required(:user_id).filled(type?: Integer, resource_exists?: User)
      required(:car_id).filled(type?: Integer, resource_exists?: Car)
    end


  end
end