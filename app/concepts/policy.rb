class MyPolicy
  def initialize(user, model)
    @user = user
    @model = model
  end

  def newuser?
    @user.has_role?(:newuser)
  end

  def admin?
    @user.has_role?(:admin)
  end
  
end