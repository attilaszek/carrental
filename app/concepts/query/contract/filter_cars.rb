require 'reform/form/dry'

module Query::Contract
  class FilterCars < Reform::Form
    feature Reform::Form::Dry

    property :start_date, virtual: true
    property :end_date, virtual: true
    property :name, virtual: true
    property :weight_min, virtual: true
    property :weight_max, virtual: true
    property :color, virtual: true
    property :rate_min, virtual: true
    property :rate_max, virtual: true

    validation :default do
  
      configure do
        config.messages_file = 'config/error_messages.yml'
        config.namespace = :reservation
      end

      required(:start_date).maybe(type?: Date, gteq?: Date.today)
      required(:end_date).maybe(type?: Date)
  
      validate(correct_time_interval?: [:start_date, :end_date]) do |start_date, end_date|
        if (start_date and end_date)
          start_date <= end_date
        else
          true
        end
      end      

      required(:name).maybe(type?: String)
      required(:color).maybe(type?: String, included_in?: Car.colors.keys)
      required(:weight_min).maybe(type?: Integer)
      required(:weight_max).maybe(type?: Integer)

      required(:rate_min).maybe(type?: Integer)
      required(:rate_max).maybe(type?: Integer)
    end

  end
end