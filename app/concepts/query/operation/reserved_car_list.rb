require 'trailblazer'

class Query::ReservedCarList < Trailblazer::Operation
  step :model!
  step Policy::Pundit( QueryPolicy, :admin? )

  def model!(options, params:, **)
    options["model"] = Car.all.select do |car| 
      !Reservation.where(car_id: car.id)
        .where(user_id: params[:user_id]).empty?
    end
  end
end