require 'trailblazer'

class Query::UserList < Trailblazer::Operation
  step :model!
  step Policy::Pundit( QueryPolicy, :admin? )

  def model!(options, **)
    options["model"] = User.all.select { |user| !user.reservations.empty? }
  end
end