require 'trailblazer'

class Query::FilterCars < Trailblazer::Operation
  step :model!
  step Policy::Pundit( QueryPolicy, :newuser? )
  step Contract::Build( constant: Query::Contract::FilterCars )
  step Contract::Validate( )
  step :filter_by_date!
  step :filter_by_name!
  step :filter_by_weight!
  step :filter_by_color!
  step :filter_by_rate!

  def model!(options, params:, **)
    options["model"] = Car.all
  end

  def filter_by_date!(options, params:, **)
    if params[:start_date].present? and params[:end_date].present?
      options["model"] = options["model"].select {|car| car.check_availability(params[:start_date], params[:end_date])}
    end
    true
  end

  def filter_by_name!(options, params:, **)
    if params[:name]
      options["model"] = options["model"].select { |car| car.name.include? params[:name] }
    end
    true
  end

  def filter_by_weight!(options, params:, **)
    if params[:weight_min]
      options["model"] = options["model"].select { |car| car.weight >= params[:weight_min] }
    end
    if params[:weight_max]
      options["model"] = options["model"].select { |car| car.weight <= params[:weight_max] }
    end
    true
  end

  def filter_by_color!(options, params:, **)
    if params[:color]
      options["model"] = options["model"].select { |car| car.color == params[:color]}
    end
    true
  end

  def filter_by_rate!(options, params:, **)
    if params[:min_rate]
      options["model"] = options["model"].select { |car| car.rate_average >= params[:min_rate] }
    end
    if params[:max_rate]
      options["model"] = options["model"].select { |car| car.rate_average <= params[:max_rate] }
    end
    true
  end
end