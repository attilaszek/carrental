require 'reform/form/dry'

module User::Contract
  class ChangePassword < Reform::Form
    feature Reform::Form::Dry

    property :password
    property :password_confirmation

    validation :default do
      required(:password).filled(min_size?: 6).confirmation
    end
  end
end