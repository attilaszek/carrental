require 'reform/form/dry'

module User::Contract
  class Update < Reform::Form
    feature Reform::Form::Dry

    property :first_name
    property :last_name

    validation :default do

      required(:first_name).maybe
      required(:last_name).maybe
    end
  end
end