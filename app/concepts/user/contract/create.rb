require 'reform/form/dry'

module User::Contract
  class Create < Reform::Form
    feature Reform::Form::Dry

    property :first_name
    property :last_name
    property :email
    property :password
    property :password_confirmation

    validation :default, with: {form: true} do

      configure do
        config.messages_file = 'config/error_messages.yml'
        config.namespace = :user

        def unique?(value)
          User.find_by(email: value).nil?
        end
      end

      required(:first_name).filled
      required(:last_name).filled
      required(:email).filled(:unique?, format?: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i)
      required(:password).filled(min_size?: 6)
      required(:password_confirmation).filled

      validate(password_confirm?: [:password, :password_confirmation]) do |pwd, pwd_conf|
        pwd.eql? pwd_conf
      end
    end
  end
end