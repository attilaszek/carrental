require "trailblazer"

class User::Index < Trailblazer::Operation

  step :model!
  step Policy::Pundit( UserPolicy, :admin? )

  def model!(options, **)
    options["model"] = User.all
  end
end