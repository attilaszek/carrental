require "trailblazer"

class User::Delete < Trailblazer::Operation
  step Model(User, :find_by)
  step Policy::Pundit( UserPolicy, :can_access? )
  step :destroy!

  def destroy!(options, model:, **)
    model.destroy
  end
end