require "trailblazer"

class User::Create < Trailblazer::Operation
  step Model( User, :new )
  step Contract::Build( constant: User::Contract::Create )
  step Contract::Validate( )
  step Contract::Persist( )
  step :add_default_role!

  def add_default_role!(optons, model:, **)
    model.add_role(:newuser)
  end
end