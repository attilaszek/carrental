require "trailblazer"

class User::Update < Trailblazer::Operation
  step Model( User, :find_by)
  step Policy::Pundit( UserPolicy, :can_access? )
  step Contract::Build( constant: User::Contract::Update )
  step Contract::Validate( )
  step Contract::Persist( )
end