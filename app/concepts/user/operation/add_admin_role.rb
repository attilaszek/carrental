require "trailblazer"

class User::AddAdminRole < Trailblazer::Operation
  step Model(User, :find_by)
  step Policy::Pundit( UserPolicy, :admin? )
  step :add_role!

  def add_role!(options, model:, **)
    model.add_role :admin
  end
end