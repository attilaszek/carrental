require "trailblazer"

class User::Authorize < Trailblazer::Operation
  step :split_header!
  step :decode_token!
  failure :report_token_error!
  step :model!


  def split_header!(options, params:, **)
    if params[:headers] and params[:headers]['Authorization']
      options["token"] = params[:headers]['Authorization'].split(' ').last
    end
  end

  def report_token_error!(options, **)
    options["errors"] = [ { token: 'Missing or invalid token'} ]
  end

  def decode_token!(options, token:, **)
    options["decoded_auth_token"] ||= JsonWebToken.decode(token)
  end

  def model!(options, decoded_auth_token:, **)
    options["model"] ||= User.find(decoded_auth_token["user_id"]) 
  end
end