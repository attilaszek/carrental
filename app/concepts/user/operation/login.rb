require "trailblazer"

class User::Login < Trailblazer::Operation
  step :model!
  step :authenticate!
  step :generate_token 

  def model!(options, params:, **)
    options["model"] = User.find_by_email(params[:email])
    if !options["model"]
      options["errors"] = {email: 'Invalid email'}
      return false
    end
    true
  end

  def authenticate!(options, params:, **)
    if !(options["model"].authenticate(params[:password]))
      options["errors"] = {password: 'Invalid password'}
      return false
    end
    true
  end

  def generate_token(options, **)
    options["token"] = JsonWebToken.encode(user_id: options["model"].id)
  end
end