require "trailblazer"

class User::ChangePassword < Trailblazer::Operation
  step Model( User, :find_by)
  step Policy::Pundit( UserPolicy, :can_access? )
  step Contract::Build( constant: User::Contract::ChangePassword )
  step Contract::Validate( )
  step Contract::Persist( )
end