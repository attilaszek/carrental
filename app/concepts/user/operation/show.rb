require "trailblazer"

class User::Show < Trailblazer::Operation
  step Model(User, :find_by)
  step Policy::Pundit( UserPolicy, :can_access? )
end