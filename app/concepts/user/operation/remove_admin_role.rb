require "trailblazer"

class User::RemoveAdminRole < Trailblazer::Operation
  step Model(User, :find_by)
  step Policy::Pundit( UserPolicy, :admin? )
  step :remove_role!

  def remove_role!(options, model:, current_user:, **)
    if current_user.id == model.id
      options["errors"] = [ { user: 'Cannot remove your own admin role'} ]
      return false 
    end
    model.remove_role :admin
  end
end