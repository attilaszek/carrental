class UserPolicy < MyPolicy

  def can_access?
    @user.has_role?(:admin) or (@user.has_role?(:newuser) and @user == @model)
  end
  
end