class RemovePhotoFromCars < ActiveRecord::Migration[5.2]
  def change
    remove_column :cars, :photo, :string
  end
end
