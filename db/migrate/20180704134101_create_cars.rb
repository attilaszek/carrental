class CreateCars < ActiveRecord::Migration[5.2]
  def change
    create_table :cars do |t|
      t.string :model
      t.integer :color
      t.integer :weight
      t.float :latitude
      t.float :longitude
      t.boolean :availability

      t.timestamps
    end
  end
end
