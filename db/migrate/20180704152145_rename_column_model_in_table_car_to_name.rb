class RenameColumnModelInTableCarToName < ActiveRecord::Migration[5.2]
  def change
    rename_column :cars, :model, :name
  end
end
