ActiveSupport::HashWithIndifferentAccess

module Paperclip
  class HashWithIndifferentAccessUploadedFileAdapter < AbstractAdapter

    def initialize(target)
      @tempfile, @content_type, @size = target["tempfile"], target["type"], target["tempfile"].size
      self.original_filename = target["filename"]
    end

  end
end

Paperclip.io_adapters.register Paperclip::HashWithIndifferentAccessUploadedFileAdapter do |target|
  target.is_a? ActiveSupport::HashWithIndifferentAccess
end